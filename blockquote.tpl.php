<?php

/**
 * @file
 * Blockquote template.
 *
 * See https://stackoverflow.com/a/49212532/836995
 * and https://stackoverflow.com/a/32171048/836995 .
 */
?>
<blockquote <?php echo $blockquote_attributes; ?>>
  <?php if ($quote): ?>
  <p><?php echo $quote; ?></p>
  <?php endif; ?>
  <?php if ($attribution): ?>
  <cite><?php echo $attribution; ?></cite>
  <?php endif; ?>
</blockquote>
